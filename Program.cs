﻿using System;

namespace DigitProduct
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static int digitsProduct(int product) {
            if(product == 0) return 10;
            if(product == 1) return 1;

            var divisor = 10;
            var power = 1;
            var result = 0;

            while(product > 1){
                if(--divisor == 1) return -1;
                while(product % divisor == 0){
                    product /= divisor;
                    result += divisor * power;
                    power *= 10;
                }
            }
            return result;
        }
    }
}
